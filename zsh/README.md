# ZSH
---
## Plugins

### Available
#### Misc.
+ TMUX
+ ssh-agent
+ Heroku
+ lein
+ command-not-found
+ gpg-agent
+ history
+ [alias-tips](https://github.com/djui/alias-tips) -- Suggests aliases.
+ [enhancd](https://github.com/b4b4r07/enhancd) --Adds navigation options to cd.
+ [zsh-bd](https://github.com/Tarrasch/zsh-bd) -- Helps navigate directories.

#### Drupal Plugins
+ [drush_zsh_completion](https://github.com/webflo/drush_zsh_completion) -- Adds drush plugin.

#### Tools
+ [calc.plugin.zsh](https://github.com/arzzen/calc.plugin.zsh) -- Calculator

#### NodeJS Plugins
+ [node]()
+ [npm]()

#### ZSH Plugins
+ [zsh-completions]()
+ [zsh-syntax-highlighting]() -- ZSH syntax highlighting/
+ [copyzshell](https://github.com/rutchkiwi/copyzshell) -- Copies zsh to another computer over ssh.

#### Python Plugins
+ [pip]()

#### Ruby/Rails Plugins
+ [ruby]()
+ [rvm]()
+ [rails]()
+ [rake]()

#### Git Plugins
+ [git]()
+ [antigen-git-rebase](https://github.com/smallhadroncollider/antigen-git-rebase) -- Guide to help rebase.
+ [antigen-git-store](https://github.com/smallhadroncollider/antigen-git-store) -- Helps sync git project amongst 2 computers.
+ [browse-commit](https://github.com/adolfoabegg/browse-commit) --  Shows past commits.
+ [git-aliases.zsh](https://github.com/peterhurford/git-aliases.zsh) -- Adds additional aliases for git.
+ [gitignore.plugin.zsh](https://github.com/voronkovich/gitignore.plugin.zsh) -- Helps create .gitignore files.
+ [rimraf/k](https://github.com/rimraf/k) -- Adds Color to git directory.

#### MySQL Plugins
+ [mysql-colorize](https://github.com/horosgrisa/mysql-colorize) -- Adds colors to MySQL Tables.
+ [mysql.plugin.zsh](https://github.com/voronkovich/mysql.plugin.zsh)

### Enabled

+ TMUX
+ ssh-agent
+ command-not-found
+ history
+ [alias-tips](https://github.com/djui/alias-tips) -- Suggests aliases.
+ [zsh-bd](https://github.com/Tarrasch/zsh-bd) -- Helps navigate directories.
+ [drush_zsh_completion](https://github.com/webflo/drush_zsh_completion) -- Adds drush plugin.
+ [calc.plugin.zsh](https://github.com/arzzen/calc.plugin.zsh) -- Calculator
+ [node]()
+ [npm]()
+ [zsh-completions]()
+ [zsh-syntax-highlighting]() -- ZSH syntax highlighting/
+ [copyzshell](https://github.com/rutchkiwi/copyzshell) -- Copies zsh to another computer over ssh.
+ [pip]()
+ [ruby]()
+ [rvm]()
+ [rails]()
+ [rake]()
+ [git]()
+ [antigen-git-rebase](https://github.com/smallhadroncollider/antigen-git-rebase) -- Guide to help rebase.
+ [antigen-git-store](https://github.com/smallhadroncollider/antigen-git-store) -- Helps sync git project amongst 2 computers.
+ [browse-commit](https://github.com/adolfoabegg/browse-commit) --  Shows past commits.
+ [git-aliases.zsh](https://github.com/peterhurford/git-aliases.zsh) -- Adds additional aliases for git.
+ [gitignore.plugin.zsh](https://github.com/voronkovich/gitignore.plugin.zsh) -- Helps create .gitignore files.
+ [rimraf/k](https://github.com/rimraf/k) -- Adds Color to git directory.
+ [mysql-colorize](https://github.com/horosgrisa/mysql-colorize) -- Adds colors to MySQL Tables.
+ [mysql.plugin.zsh](https://github.com/voronkovich/mysql.plugin.zsh)

---

## Themes

### Available
+ [robbyrussell]()
+ [fox]()
+ [awesomepanda]()
+ [muse]()
+ [josh]()
+ [bureau]()
+ [jreese]()

### Enabled
+ [jreese]()

